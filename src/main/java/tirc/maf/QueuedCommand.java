package tirc.maf;

/**
 * If a queue command cannot be executed immediately, it will be put in a queue to execute later.
 * <p>
 * The queue command has the priority attribute. 0 is the highest priority and 255 is the lowest priority.
 * 
 * @since 0.1
 * @author ryan_hsieh
 * @see MAFCommand
 * 
 */
public abstract class QueuedCommand extends MAFCommand implements Comparable<QueuedCommand>{

	private int priority = 100;  //0~255, 0:highest  255:lowest

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		if (priority>255) priority = 255;
		if (priority<0) priority = 0;
		
		this.priority = priority;
	}
	
	public int compareTo (QueuedCommand o) {
		return 	(getPriority()-o.getPriority());
	}

}
