package tirc.maf;

public enum MAFCommandReceivedResult {
	ACCEPT,
	BUSY,
	UNKNOWN
}
