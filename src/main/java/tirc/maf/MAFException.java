package tirc.maf;

public class MAFException extends Exception {

	private static final long serialVersionUID = -358608327004741708L;
	public static int ERROR  = -1;
	public static int PORT_IN_USE	 = -2;
	
	public int errcode = ERROR;
	
	public MAFException(int errcode, String message) {
		super(message);
		this.errcode = errcode;
	}
	public MAFException(int errcode, String message, Throwable cause) {
		super(message, cause);
		this.errcode = errcode;
	}
	public MAFException(String message) {
		super(message);
	}
	
	public int getErrCode() {
		return this.errcode;
	}
	
}
