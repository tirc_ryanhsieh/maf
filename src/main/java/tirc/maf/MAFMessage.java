package tirc.maf;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is the data package transmited between client and server. 
 * <p>
 * MAFMessage contains two parts (a message string and attachments).
 * <p>
 * If a meesage string in MAFMessage contains a comma, a string before first comma called a command string and a string after first comma called a parameter string.
 * <p>
 * For example,
 * <br>
 * a message "hello, are you there", a command string will be "hello" and a parameter string will be "are you there".
 * <p>
 * One attachement is byte[] data. MAFMessage allows to put multiple attachemets in it.
 * 
 * @author ryan_hsieh
 * @since 0.1
 *
 */
public class MAFMessage {
	private String clientId = "";
	private String message = "";
	private List<byte[]> attachments = new ArrayList<byte[]>();
	
	
	public MAFMessage() {		
	}
	
	/**
	 * Constructs MAFMessage with a message string.
	 * 
	 * @param msg a message string.
	 */
	public MAFMessage(String msg) {
		this.message = msg;
	}

	public String getClientId() {
		return clientId;
	}

	/**
	 * Assigns a client identity.
	 * <p>
	 * The server will dispatch a message with ClientID to the specified client and broadcast a message without ClientID to all clients.
	 * 
	 * @param clientId the client identity.
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getMessage() {
		return message;
	}

	/**
	 * Set the message string
	 * 
	 * @param message the message string
	 */
	public void setMessage(String message) {
		this.message = message;
	}
		
	/**
	 * Get the command part of the message string.
	 * <p>
	 * here is some examples:
	 * <br>
	 * <ul>
	 * <li> A message "LED.SWITCH,ENABLE,10SEC". The command is "LED.SWITCH" and the parameter is "ENABLE,10SEC".
	 * <li> A message "LED.QUERY". the command is "LED.QUERY" and the parameter is empty.
	 *  </ul>
	 * 
	 * @return the command string
	 */
	public String getCommand() {
		int pos = message.indexOf(",");
		if (pos>=0) {
			return message.substring(0, pos).trim();
		} else {
			if (!message.isEmpty()) {
				return message.trim();
			}
		}
		return "";
	}
	
	/**
	 * Get the parameter part of the message string.
	 * 
	 * @return the parameter string
	 */
	public String getParamString() {
		int pos = message.indexOf(",");
		if (pos>=0) {
			return message.substring(pos+1).trim();
		}
		return "";
	}
	
	/**
	 * Put one attachment in this MAFMessage.
	 * 
	 * @param attachment byte array data 
	 */
	public void putAttachment(byte[] attachment) {
		this.attachments.add(attachment);
	}
	
	/**
	 * Get a attachemtn at the specified index.
	 * 
	 * @param index attachment index 
	 * @return an attachment at the specified index 
	 */
	public byte[] getAttachment(int index) {
		return this.attachments.get(index);
	}
	
	/**
	 * Get the number of attachments in this MAFMessage.
	 *  
	 * @return the attachemnt number
	 */
	public int getAttachmentNum() {
		return this.attachments.size();
	}

}
