package tirc.maf;

import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

/**
 * This class creates a maf server.
 * <p>
 * One MAServer can communicate with multiple MAFClients at the same time.
 * MAFServer connects to each MAFClient with two connection channels: service and broadcast.
 * <ol>
 * <li>The service channel is bidirectional. A server and a client can communicate messages via this channel.   
 * <li>The broadcast channel is unidirectional. A server can notify all clients via this channel.
 * </ol>
 * <p>
 * MAFServer and MAFClient use MAFMessage as data package.
 * When MAFServer received a MAFMessage, MAFServer colud perform different behaviors by the message contained in MAFMessage.  
 * <p>
 * Here are some MAFMessage examples.
 * <ol>
 * <li>MAFMessage contains a message "motor.run,5,20,30".
 * <br>A word(motor.run) before first comma will be a command. A string(5,20,30) after first comma will be a parameter string. 
 * MAFServer will try to find a matching command class from comand searching path and execute it. 
 * these command classes must extend MAFCommand.
 * <li>MAFMessage contains a message "bonjour".
 * <br>MAFClient can send this message to MAFServer. When MAFServer is ready, MAFServer will send "bonjour" back immediately.
 * <li>MAFMessage contains a message "heartbeat".
 * <br>When heatbeat feature enable, MAFClient can send this meesage to inform MAFServer that client is alive.
 * </ol>
 *  
 *  <p>
 *  MAFServer will report some events via IMAFServerListener.
 *  
 *  <pre>  
 *  MAFServer sever = new MAFServer();
 *  //set command searching path	
 *  server.setCommandPackage("tirc.maf.test.cmd"); 
 *  //set listener
 *  server.setListener(new IMAFServerListener() { 
 *    public void onClientConnected(String clientId) {
 *        // some client said "bonjour"
 *    }
 *    public void onClinetHeartbeatTimeout(String clientId) {
 *        // some client heatbeat timeout 
 *    }
 *    public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
 *        // client send a message. server report accept or reject this message.
 *        // if a message return, this message will be sent to client. 
 *        // this return message can use to acknowledge receipt.
 *    }			
 *  });
 *  server.start(); 
 *  </pre>
 * 
 * @author ryan_hsieh
 * @see MAFClient
 * @see MAFCommand
 * @see MAFMessage
 * @see IMAFServerListener
 * @since 0.1
 *
 */
public class MAFServer {
	public static int DEFAULT_SERVICE_PORT   = 18201;
	public static int DEFAULT_BROADCAST_PORT = 18202;
	public static int DEFAULT_SIGNAL_PORT    = 18203;
	
	private static Logger logger = LoggerFactory.getLogger(MAFServer.class) ;
	
	private ConcurrentLinkedQueue<MAFMessage> outgoingQueue = new ConcurrentLinkedQueue<MAFMessage>();
	private IMAFCommandExecutorPool executorPool = new MAFCommandExecutorPool(this);
	private ZMQ.Context context = null;
	private Socket zmqSignalSrc = null; 
	private AtomicBoolean serviceSuspend = new AtomicBoolean(false);
	
	private int servicePort   = DEFAULT_SERVICE_PORT;
	private int broadcastPort = DEFAULT_BROADCAST_PORT;
	private int signalPort    = DEFAULT_SIGNAL_PORT;
		
	private ExecutorService serviceExec;
	private Future<Integer> serviceResult;
	private IMAFServerListener listener = null;
	private String commandPackage = "";
	
	private int heartbeatInterval = 0;
	private HashMap<String, Long> heartbeatTable = new HashMap<String, Long>();  
	
	public MAFServer() {
		this.context = ZMQ.context(1);
	}
	/**
	 * Constructs a MAFServer using arguments.
	 * 
	 * @param context an existing ZMQ.context
	 */
	public MAFServer(ZMQ.Context context) {
		this.context = context;
	}		
	
	public ZMQ.Context getContext() {
		return context;
	}
	
	public int getServicePort() {
		return servicePort;
	}
	
	/**
	 * Set the service port number.
	 *  
	 * @param servicePort service port number
	 */
	public void setServicePort(int servicePort) {
		this.servicePort = servicePort;
	}
	
	public int getBroadcastPort() {
		return broadcastPort;
	}
	
	/**
	 * Set the broadcast port number.
	 * 
	 * @param broadcastPort broadcast port number
	 */
	public void setBroadcastPort(int broadcastPort) {
		this.broadcastPort = broadcastPort;
	}
	
	public int getSignalPort() {
		return signalPort;
	}
	
	/**
	 * Set the signal port number.
	 *  
	 * @param signalPort signal port number
	 */
	public void setSignalPort(int signalPort) {
		this.signalPort = signalPort;
	}

	/**
	 * Start MAFServer.
	 * 
	 * @return A Future represents the result of the MAFServer execution. 
	 */
	public Future<Integer> start() {
		
		logger.info("start");
		
		if (null != executorPool) {
			executorPool.create();
		}
				
		serviceExec = Executors.newSingleThreadExecutor();
		Callable<Integer> task = () -> { return main_loop(); };	
		serviceResult = serviceExec.submit(task);
				
		
		return serviceResult;
	}
	
	/**
	 * Terminate MAFServer.
	 */
	public void terminate() {
		
		logger.info("terminate");
				
		if (null != executorPool) {
			executorPool.destroy();
		}		

		if (isSuspend()) sendSignal("exit");
		
		try {
			serviceResult.get(5, TimeUnit.SECONDS);
			serviceExec.shutdown();
			serviceExec.awaitTermination(5, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.error(e.getClass().getSimpleName()+": "+e.getMessage());
		    e.printStackTrace();
		}
		finally {
		    if (!serviceExec.isTerminated()) {		       
			    serviceExec.shutdownNow();
		    }
		}			
	}
	
	/**
	 * Set command searching path.
	 * <p>
	 * set the package name to "tirc.cmd", the class of a command "motor.run" will be "tirc.cmd.MOTOR_RUN"   
	 * 
	 * @param packageName the package name of the command class
	 */
	public void setCommandPackage(String packageName) {
		this.commandPackage = packageName;
	}
	
	public IMAFCommandExecutorPool getExecutorPool() {
		return executorPool;
	}

	/**
	 * Assign a custom command executor pool.
	 * 
	 * @param executorPool the custom command executor pool
	 * @see IMAFCommandExecutorPool
	 */
	public void setExecutorPool(IMAFCommandExecutorPool executorPool) {
		this.executorPool = executorPool;
	}
	
	/**
	 * Set a listener to receive server notification. 
	 * 
	 * @param listener a server listener
	 */
	public void setListener(IMAFServerListener listener) {
		this.listener = listener;
	}
	
	/**
	 * Enable/disable heartbeat checking.
	 * <p>
	 * if interval &gt; 0, enable heartbeat checking.
	 * <br>
	 * if interval &lt;= 0, disable heartbeat checking.
	 *  
	 * @param interval a period of heartbeat
	 */
	public void setHeartbeatInterval(int interval) {
		heartbeatInterval = interval;
		sendSignal("heatbeatIntervalChanged");
	}
	
	/**
	 * Unicast/broadcast message to client(s).
	 * <p>
	 * if the message contains the client id, this message will be delivered to the specified client.
	 * <br>
	 * if the message contains no client id, this message will be sent to each client.
	 * 
	 * @param data message to client(s)
	 */
	public void send(MAFMessage data) {
		outgoingQueue.offer(data);
		
		if (isSuspend()) sendSignal("dataReady");
	}
	
	/**
	 * Get the the result of the MAFServer execution.
	 * 
	 * @return A Future represents the result of the MAFServer execution.
	 */
	public Future<Integer> getResult() {
		return this.serviceResult;
	}
	
	/**
	 * Send signal to server.
	 * <p> 
	 * Other modules can use this function to send a signal string to server. 
	 * 
	 * @param info a signal string
	 */
	public synchronized void sendSignal(String info) {
		if (null != this.zmqSignalSrc) 	this.zmqSignalSrc.send(info);
	}
	
	/**
	 * Check if server is suspended.
	 * <p> 
	 * when there is no message to send or receive, the server will enter the suspend mode.
	 * 
	 * @return a boolean value represents if the server is suspended or not.
	 */
	public boolean isSuspend() {
		return serviceSuspend.get();
	}
	
	private int main_loop() {
		
		logger.info("server running");
		int ret = 0;
		boolean bExit = false;
		Socket zmqService = null;
		Socket zmqBroadcast = null;
		Socket zmqSignalDest = null;

		try {
			zmqService = context.socket(ZMQ.ROUTER);
			zmqService.setRouterHandover(true);
			zmqService.setLinger(0);
			try {
				zmqService.bind(String.format("tcp://*:%d", servicePort));
			} catch (org.zeromq.ZMQException ex) {
				String msg = String.format("server service port(%d) in use.", servicePort);
				throw new MAFException(MAFException.PORT_IN_USE, msg, ex);
			}

			zmqBroadcast = context.socket(ZMQ.PUB);
			zmqBroadcast.setLinger(0);
			try {
				zmqBroadcast.bind(String.format("tcp://*:%d", broadcastPort));
			} catch (org.zeromq.ZMQException ex) {
				String msg = String.format("server broadcast port(%d) in use.", broadcastPort);
				throw new MAFException(MAFException.PORT_IN_USE, msg, ex);
			}

			// zmqSignalDest and zmqSignalSrc built a channel.
			// When other modules send signal messages via zmqSignalSrc,
			// zmqSignalDest will receive these signal messages.
			// These signal messages can be used to wake up the service, notify
			// to finish the service, etc.
			// REMARK: Since zmqsocket is not threadsafe, zmqSignalSrc must be
			// used carefully.
			zmqSignalDest = context.socket(ZMQ.PAIR);
			try {
				zmqSignalDest.bind(String.format("tcp://localhost:%d", signalPort));
			} catch (org.zeromq.ZMQException ex) {
				String msg = String.format("server signal port(%d) in use.", signalPort);
				throw new MAFException(MAFException.PORT_IN_USE, msg, ex);
			}
			zmqSignalDest.setLinger(0);
			zmqSignalSrc = context.socket(ZMQ.PAIR);
			zmqSignalSrc.connect(String.format("tcp://localhost:%d", signalPort));

			Poller poller = new ZMQ.Poller(2);
			poller.register(zmqService, Poller.POLLIN);
			poller.register(zmqSignalDest, Poller.POLLIN);

			while (!bExit) {

				if (heartbeatInterval > 0) {
					poller.poll(heartbeatInterval);
					checkHeartbeatTable();
				} else {
					if (outgoingQueue.isEmpty()) {
						serviceSuspend.set(true);
						poller.poll(-1);
						serviceSuspend.set(false);
					} else {
						poller.poll(0);
					}
				}

				if (poller.pollin(0)) { // service
					MAFMessage inMsg = recvMessage(zmqService);
					boolean done = processSpecificMessage(inMsg);
					if (!done) 	processGeneralMessage(inMsg);
				}

				if (poller.pollin(1)) { // signal
					String msg = zmqSignalDest.recvStr();
					switch (msg) {
					case "exit":
						bExit = true;
						break;
					default: // wake-up purpose
						break;
					}
				}
				// check out message queue
				checkOutgoingQueue(zmqService, zmqBroadcast);
			}

		} catch (MAFException ex) {
			logger.error(ex.getMessage(), ex.getCause());
			ret = ex.getErrCode();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			ret = MAFException.ERROR;
		} finally {
			if (null != zmqService)		zmqService.close();
			if (null != zmqBroadcast) 	zmqBroadcast.close();
			if (null != zmqSignalDest) 	zmqSignalDest.close();
			if (null != zmqSignalSrc) 	zmqSignalSrc.close();
		}
		logger.info("server exit");
		return ret;
	}
	
	private MAFMessage recvMessage(Socket zmqService) {
		MAFMessage msg = new MAFMessage();				
		ZMsg zmsg = ZMsg.recvMsg(zmqService);
		int  size = zmsg.size();				
		for (int i=0; i<size; i++) {					
			ZFrame zframe = zmsg.pop();
			switch(i) {
			case 0:  //client id
				msg.setClientId(zframe.toString());
				break;
			case 1:  //message
				msg.setMessage(zframe.toString());
				break; 
			default: //attachments
				msg.putAttachment(zframe.getData());
				break;
			}					
			zframe.destroy();
		}                
        zmsg.destroy();                       
		return msg;
	}

	private void updateHeartbeatTable(String clientId) {
		this.heartbeatTable.put(clientId, System.currentTimeMillis());
	}
	
	private void checkHeartbeatTable() {
		
		if (this.heartbeatInterval<=0) {
			this.heartbeatTable.clear();
			return;
		}
		
		long curTime = System.currentTimeMillis();
		Set<String> clients = this.heartbeatTable.keySet();		
		for (String cid: clients) {
			long lastUpdateTime = heartbeatTable.get(cid);
			if ((curTime-lastUpdateTime)>this.heartbeatInterval) {
				if (null != listener) {
					listener.onClinetHeartbeatTimeout(cid);
				}
				heartbeatTable.remove(cid);
			}
		}		
	}

	private boolean processSpecificMessage(MAFMessage inMsg) {
		String cmd = inMsg.getCommand();
		switch(cmd.toLowerCase()) {
		case "bonjour":
			MAFMessage outMsg = new MAFMessage();
			outMsg.setClientId(inMsg.getClientId());
			outMsg.setMessage(cmd);
			outgoingQueue.offer(outMsg);			
			if (null != listener) {
				listener.onClientConnected(inMsg.getClientId());
			}			
			return true;
		case "heartbeat":			
			String cid = inMsg.getClientId();			
			updateHeartbeatTable(cid);
			return true;
		}
		return false;
	}
	
	private void processGeneralMessage(MAFMessage inMsg) {
		// pass message to running commands
		boolean done = executorPool.dispatch(inMsg);
		// if return true, this message has been processed by a running command.
		if (done) return;   
		
		// try to convert a message to a command object
        MAFCommand cmd = createCommand(inMsg);
        if (null==cmd) {
        	if (null != listener) {
        		MAFMessage ackmsg = listener.onMessageReceived(inMsg, MAFCommandReceivedResult.UNKNOWN);
        		if (null != ackmsg) {
        			ackmsg.setClientId(inMsg.getClientId());
        			outgoingQueue.offer(ackmsg);
        		}
        	}
        } else {
        	if (executorPool.accept(cmd)) {
        		if (null != listener) {
            		MAFMessage ackmsg = listener.onMessageReceived(inMsg, MAFCommandReceivedResult.ACCEPT);
            		if (null != ackmsg) {
            			ackmsg.setClientId(inMsg.getClientId());
            			outgoingQueue.offer(ackmsg);
            		}
        		}
        		if (!executorPool.dispatch(cmd)) {
        			logger.error("failed to dispatch command({})",cmd.getClass().getSimpleName());
        		}
        	} else {
        		if (null != listener) {
            		MAFMessage ackmsg = listener.onMessageReceived(inMsg, MAFCommandReceivedResult.BUSY);
            		if (null != ackmsg) {
            			ackmsg.setClientId(inMsg.getClientId());
            			outgoingQueue.offer(ackmsg);
            		}
        		}                		
        	}
        }                                          		
	}
	
	private void checkOutgoingQueue(Socket zmqService, Socket zmqBroadcast) {
		
		if (!outgoingQueue.isEmpty()) {
			
			Socket socket;
			MAFMessage outmsg = outgoingQueue.poll();
			String cid = outmsg.getClientId();
			String msg = outmsg.getMessage();
			if (null!=cid && !cid.isEmpty()) {
				zmqService.sendMore(cid);
				socket = zmqService;
			} else {
				socket = zmqBroadcast;
			}

			int attachNum = outmsg.getAttachmentNum(); 
			if (0==attachNum) {
				socket.send(msg);
			} else {
				socket.sendMore(msg);
			}
			
			for (int i=0; i<attachNum; i++) {
				if ((attachNum-1)==i) {
					socket.send(outmsg.getAttachment(i));
				} else {
					socket.sendMore(outmsg.getAttachment(i));
				}
			}
		}		
	}
	
	private MAFCommand createCommand(MAFMessage inMessage)
	{
		try {
			String clsname = inMessage.getCommand().replace(".", "_");
			String fullclsname = this.commandPackage+"."+clsname.toUpperCase();
			Class<?> taskClass = Class.forName(fullclsname);
			Object obj = taskClass.newInstance();	
			if (obj instanceof MAFCommand) { 
				MAFCommand cmd = (MAFCommand)obj;
				cmd.setOriginalMessage(inMessage);				
				return cmd;
			}
		} catch (Exception e) {
			logger.error(e.getClass().getSimpleName()+": "+e.getMessage());
			e.printStackTrace();
		} 
    	return null;
	}	
}
