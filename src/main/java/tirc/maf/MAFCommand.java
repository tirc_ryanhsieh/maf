package tirc.maf;

/**
 * This abstract class represents an executing unit in MAFServer. 
 * 
 * @author ryan_hsieh
 * @since  0.1
 *
 */
public abstract class MAFCommand {
	
	private MAFCommandExecutor executor = null;
	private MAFMessage originalMessage;
		
	public MAFCommandExecutor getExecutor() {
		return executor;
	}
	/**
	 * Set the command executor.
	 * 
	 * @param executor the command executor.
	 */
	public void setExecutor(MAFCommandExecutor executor) {
		this.executor = executor;
	}

	/**
	 * Send message to the source client.
	 * 
	 * @param msg a message string.
	 */
	public void send(String msg) {
		MAFMessage outMsg = new MAFMessage();
		outMsg.setClientId(originalMessage.getClientId());
		outMsg.setMessage(msg);
		executor.send(outMsg);
	}	
	/**
	 * Send messag to the source client.
	 * 
	 * @param data a message object.
	 */
	public void send(MAFMessage data) {
		data.setClientId(originalMessage.getClientId());
		executor.send(data);
	}
	
	/**
	 * Broadcast message to all clients.
	 * 
	 * @param msg a message string
	 */
	public void broadcast(String msg) {
		MAFMessage outMsg = new MAFMessage();
		outMsg.setMessage(msg);
		executor.send(outMsg);
	}	
	/**
	 * Broadcast message to all clients.
	 * 
	 * @param data a message object
	 */
	public void broadcast(MAFMessage data) {	
		data.setClientId("");
		executor.send(data);
	}
	
	/**
	 * Get the original message to create this command.
	 * 
	 * @return a message object
	 */
	public MAFMessage getOriginalMessage() {
		return originalMessage;
	}
	
	/**
	 * Set the original message to create this command.
	 * 
	 * @param originalMessage a message object
	 */
	public void setOriginalMessage(MAFMessage originalMessage) {
		this.originalMessage = originalMessage;
	}

	/**
	 * Process an incoming message when this command is executing.
	 * <p>
	 * If a command needs to receive an incoming message, it shall override this function.
	 * 
	 * @param message an incoming message
	 * @return true if this command decalres this message doesn't need to be processed further, else otherwise.
	 */
	public boolean processMessage(MAFMessage message) {
		return false;
	}
	
	/**
	 * Abstract executing function.
	 * <p>
	 * A class extending this class shall implement this abstract function.
	 */
	public abstract void execute();

}
