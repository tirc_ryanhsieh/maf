package tirc.maf;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;

/**
 * This class creates a maf client.
 * <p>
 * One MAFClient can only communicate with one MAFServer at the same time. 
 * Each client must have a uniqle identity. Default client identity is a random UUID string.
 * The client identity can be changed before client.start().
 * <p>
 * MAFClient will report some events via IMAFClientListener.
 * 
 * <pre>
 * MAFClient client = new MAFClient();
 * // set server address
 * client.setServerAddress("127.0.0.1");
 * // set listener
 * client.setListener(new IMAFClientListener() {
 *     public void onServiceMessageReceived(MAFMessage msg) {
 *         // when received a unicast message from server
 *     }
 *     public void onBroadcastMessageReceived(MAFMessage msg) {
 *         // when received a broadcast message from server
 *     }
 * });
 * client.start();
 * </pre>
 * 
 * @author ryan_hsieh
 * @see MAFServer
 * @see MAFMessage
 * @see IMAFClientListener
 * @since 0.1
 */
public class MAFClient {
	private static Logger logger = LoggerFactory.getLogger(MAFClient.class) ;

	public static int DEFAULT_SIGNAL_PORT = 35000;
	
	private ConcurrentLinkedQueue<MAFMessage> outgoingQueue = new ConcurrentLinkedQueue<MAFMessage>();

	private String cid = UUID.randomUUID().toString();
	private ExecutorService serviceExec;
	private Future<Integer> serviceResult;
	private int servicePort   = MAFServer.DEFAULT_SERVICE_PORT;
	private int broadcastPort = MAFServer.DEFAULT_BROADCAST_PORT;
	private int signalPort    = DEFAULT_SIGNAL_PORT;
	private String serverAddress = "localhost";
	private boolean bExit = false;	
	private IMAFClientListener listener = null;
	private ZMQ.Context context = null;	
	private Socket zmqSignalSrc = null; 
	private AtomicBoolean serviceSuspend = new AtomicBoolean(false);
	
	public MAFClient() {
		this.context = ZMQ.context(1);
	}
	/**
	 * Constructs a MAFServer using arguments.
	 * 
	 * @param context an existing ZMQ.context
	 */	
	public MAFClient(ZMQ.Context context) {	
		this.context = context;
	}
	
	/**
	 * Constructs a MAFServer using arguments.
	 * 
	 * @param context an existing ZMQ.context
	 * @param cid a new client identity 
	 */
	public MAFClient(ZMQ.Context context, String cid) {
		this.context = context;
		this.cid = cid;
	}

	public String getClientId() {
		return cid;
	}
	
	/**
	 * Set a client listener.
	 * 
	 * @param listener a client listener
	 */
	public void setListener(IMAFClientListener listener) {
		this.listener = listener;
	}
	
	public String getServerAddress() {
		return serverAddress;
	}

	/**
	 * Set the server address.
	 * 
	 * @param serverAddress the server address
	 */
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	public int getServicePort() {
		return servicePort;
	}
	
	/**
	 * Set the service port number.
	 *  
	 * @param servicePort service port number
	 */	
	public void setServicePort(int servicePort) {
		this.servicePort = servicePort;
	}	
	
	public int getBroadcastPort() {
		return broadcastPort;
	}

	/**
	 * Set the broadcast port number.
	 *  
	 * @param broadcastPort broadcast port number
	 */	
	public void setBroadcastPort(int broadcastPort) {
		this.broadcastPort = broadcastPort;
	}

	public int getSignalPort() {
		return signalPort;
	}

	/**
	 * Set the signal port number.
	 *  
	 * @param signalPort signal port number
	 */	
	public void setSignalPort(int signalPort) {
		this.signalPort = signalPort;
	}
	
	/**
	 * Start MAFClient.
	 *  
	 * @return A Future represents the result of the MAFClient execution. 
	 */	
	public Future<Integer> start() {		

		serviceExec = Executors.newSingleThreadExecutor();
		Callable<Integer> task = () -> { return main_loop(); };			
		serviceResult = serviceExec.submit(task); 
				
		return serviceResult;			
	}

	/**
	 * Terminate MAFClient.
	 */
	public void terminate() {
		
		if (isSuspend()) sendSignal("exit");
		
		try {
			serviceResult.get(5, TimeUnit.SECONDS);
			serviceExec.shutdown();
			serviceExec.awaitTermination(5, TimeUnit.SECONDS);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		finally {
		    if (!serviceExec.isTerminated()) {		       
			    serviceExec.shutdownNow();
		    }
		}			
	}
	
	/**
	 * Send a message to the server
	 *  
	 * @param msg a message string
	 */
	public void send(String msg) {
		MAFMessage data = new MAFMessage();
		data.setMessage(msg);
		outgoingQueue.offer(data);
		
		if (isSuspend()) sendSignal("dataReady");
	}
	
	/**
	 * Send a message to the server
	 * 
	 * @param data a message object
	 */
	public void send(MAFMessage data) {
		outgoingQueue.offer(data);

		if (isSuspend()) sendSignal("dataReady");
	}

	/**
	 * Get the the result of the MAFClient execution.
	 * 
	 * @return A Future represents the result of the MAFClient execution.
	 */
	public Future<Integer> getResult() {
		return this.serviceResult;
	}
	
	/**
	 * Send signal to the client.
	 * <p> 
	 * Other modules can use this function to send a signal string to the client. 
	 * 
	 * @param info a signal string
	 */	
	public synchronized void sendSignal(String info) {
		this.zmqSignalSrc.send(info);
	}
	
	/**
	 * Check if the client is suspended.
	 * <p> 
	 * when there is no message to send or receive, the client will enter the suspend mode.
	 * 
	 * @return a boolean value represents if the client is suspended or not.
	 */	
	public boolean isSuspend() {
		return serviceSuspend.get();
	}
	
	private int main_loop() {
		int ret = 0;

		logger.debug("client({}) running", this.getClientId());

		Socket zmqService = null;
		Socket zmqBroadcast = null;
		Socket zmqSignalDest = null;

		try {
			zmqService = context.socket(ZMQ.DEALER);
			zmqService.setIdentity(cid.getBytes());
			zmqService.connect(String.format("tcp://%s:%d", serverAddress, servicePort));

			zmqBroadcast = context.socket(ZMQ.SUB);
			zmqBroadcast.subscribe("".getBytes());
			zmqBroadcast.connect(String.format("tcp://%s:%d", serverAddress, broadcastPort));

			// zmqSignalDest and zmqSignalSrc built a channel.
			// When other modules send signal messages via zmqSignalSrc,
			// zmqSignalDest will receive these signal messages.
			// These signal messages can be used to wake up the service, notify
			// to finish the service, etc.
			// REMARK: Since zmqsocket is not threadsafe, zmqSignalSrc must be
			// used carefully.
			zmqSignalDest = context.socket(ZMQ.PAIR);
			zmqSignalDest.setLinger(0);
			try {
				zmqSignalDest.bind(String.format("tcp://localhost:%d", signalPort));
			} catch (org.zeromq.ZMQException ex) {
				String msg = String.format("client signal port(%d) in use.", signalPort);
				throw new MAFException(MAFException.PORT_IN_USE, msg, ex);
			}

			zmqSignalSrc = context.socket(ZMQ.PAIR);
			zmqSignalSrc.connect(String.format("tcp://localhost:%d", signalPort));

			Poller poller = new ZMQ.Poller(3);
			poller.register(zmqService, Poller.POLLIN);
			poller.register(zmqBroadcast, Poller.POLLIN);
			poller.register(zmqSignalDest, Poller.POLLIN);

			while (!bExit) {
				if (outgoingQueue.isEmpty()) {
					serviceSuspend.set(true);
					poller.poll(-1);
					serviceSuspend.set(false);
				} else {
					poller.poll(0);
				}

				if (poller.pollin(0)) { // service
					MAFMessage msg = recvMessage(zmqService);
					msg.setClientId(this.cid);
					if (null != listener) {
						listener.onServiceMessageReceived(msg);
					}
				}

				if (poller.pollin(1)) { // broadcast
					MAFMessage msg = recvMessage(zmqBroadcast);
					if (null != listener) {
						listener.onBroadcastMessageReceived(msg);
					}
				}

				if (poller.pollin(2)) { // signal
					String msg = zmqSignalDest.recvStr();
					switch (msg) {
					case "exit":
						bExit = true;
						break;
					default: // wake-up purpose
						break;
					}
				}

				// check out message queue
				checkOutgoingQueue(zmqService);
			}
		} catch (MAFException ex) {
			logger.error(ex.getMessage(), ex.getCause());
			ret = ex.getErrCode();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			ret = MAFException.ERROR;
		} finally {
			if (null != zmqService)		zmqService.close();
			if (null != zmqBroadcast)	zmqBroadcast.close();
			if (null != zmqSignalDest)	zmqSignalDest.close();
			if (null != zmqSignalSrc)	zmqSignalSrc.close();
		}
		logger.debug("client({}) exit", this.getClientId());

		return ret;
	}
	
	private MAFMessage recvMessage(Socket socket) {
		ZMsg zmsg = ZMsg.recvMsg(socket);
		MAFMessage msg = new MAFMessage();					
		int  size = zmsg.size();	
		for (int i=0; i<size; i++) {					
			ZFrame zframe = zmsg.pop();
			switch(i) {
			case 0:  //message
				msg.setMessage(zframe.toString());
				break; 
			default: //attachments
				msg.putAttachment(zframe.getData());
				break;
			}					
			zframe.destroy();
		}      
        zmsg.destroy();
		return msg;
	}
	
	private void checkOutgoingQueue(Socket zmqService) {
		if (!outgoingQueue.isEmpty()) {
			MAFMessage outmsg = outgoingQueue.poll();
			String msg = outmsg.getMessage();
			
			int attachNum = outmsg.getAttachmentNum(); 
			if (0==attachNum) {
				zmqService.send(msg);
			} else {
				zmqService.sendMore(msg);
			}
			
			for (int i=0; i<attachNum; i++) {
				if ((attachNum-1)==i) {
					zmqService.send(outmsg.getAttachment(i));
				} else {
					zmqService.sendMore(outmsg.getAttachment(i));
				}
			}
		}		
	}

}
