package tirc.maf;

/**
 * A direct command asks for executing at once else gives up.
 * <p>
 * The queue command has the priority attribute. 0 is the highest priority and 255 is the lowest priority.
 * 
 * @since 0.1
 * @author ryan_hsieh
 * @see MAFCommand
 * 
 */
public abstract class DirectCommand extends MAFCommand {

}
