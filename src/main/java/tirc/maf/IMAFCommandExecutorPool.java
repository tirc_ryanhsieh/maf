package tirc.maf;

/**
 * An interface represents how the server processes an incoming request.
 * <p>
 * Here is the flow for receiving a request.
 * <br>
 * <ol>
 * <li>Convert the request to the MAFMessage object.
 * <li>Invoke IMAFCommandExecutorPool.dispatch(MAFMessage).
 * <li>If no one handles MAFMessage, try to find a MAFCommand to handle this MAFMessage.
 * <li>Invoke IMAFCommandExecutorPool.accept(MAFCommand), check if any executor can accpet this command.
 * <li>If an executor can accpet this command, invoke IMAFCommandExecutorPool.dispatch(MAFCommand) to dispatch this command to the proper executor. 
 * </ol>
 * 
 * @author ryan_hsieh
 * @see MAFCommandExecutor
 * @see MAFCommand
 * @see MAFMessage
 * @since 0.1
 */
public interface IMAFCommandExecutorPool {
	/**
	 * Create proper command executors.
	 */
	public void create();
	/**
	 * Destroy command executors.
	 */
	public void destroy();
	/**
	 * Check if any command executor can accept this command.
	 * 
	 * @param command a command object
	 * @return true if a command executor can accept this command, false otherwise.
	 */
	public boolean accept(MAFCommand command);
	/**
	 * Dispatch the command to the command execuor. 
	 * 
	 * @param command a command object
	 * @return true if this command is processed sucessfully, false otherwise.
	 */
	public boolean dispatch(MAFCommand command);
	/**
	 * Dispatch the message to the executing command object.
	 * 
	 * @param message a message object
	 * @return true if this message is alreay processed by an executor, false otherwise. 
	 */
	public boolean dispatch(MAFMessage message);
}
