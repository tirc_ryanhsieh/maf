package tirc.maf;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for executing DirectCommand.
 * <p>
 * If a direct command is executing, another direct command will be rejected by this executor.
 * 
 * @since 0.1
 * @author ryan_hsieh
 * @see MAFCommandExecutor
 * @see DirectCommand
 * 
 */
public class DirectCommandExecutor extends MAFCommandExecutor {
	
	private static Logger logger = LoggerFactory.getLogger(DirectCommandExecutor.class) ;

	private ExecutorService exec;
	private Future<Integer> result;
	private boolean bExit = false;
	
	private DirectCommand runningCmd = null;
	
	public DirectCommandExecutor(MAFServer msgService) {
		super(msgService);
	}
	@Override
	public boolean processMessage(MAFMessage message) {
		if (null != runningCmd) {
			return runningCmd.processMessage(message);
		}
		return false;
	}	
	@Override
	public boolean accept(MAFCommand command) {
		if (null != runningCmd) {
			return false;
		} else {
			return true;			
		}
	}
	@Override
	public boolean execute(MAFCommand command) {			
		if (null != runningCmd) return false;
		synchronized(this) {
			runningCmd = (DirectCommand)command;
			notify();
		}
		return true;
	}

	@Override
	public void start() {
		exec = Executors.newSingleThreadExecutor();
		Callable<Integer> task = () -> { return main_loop(); };	
		result = exec.submit(task);
	}

	@Override
	public void terminate() {
		bExit = true;
		synchronized(this) {
			notify();		
		}		
		try {
			result.get(5, TimeUnit.SECONDS);
			exec.shutdown();
			exec.awaitTermination(5, TimeUnit.SECONDS);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		} finally {
		    if (!exec.isTerminated()) {		       
		    	exec.shutdownNow();
		    }
		}
	}
	
	private int main_loop() {	
		while (!bExit) {
			if (null==runningCmd) {
				synchronized(this) {
					try {
						wait();
					} catch (InterruptedException ex) {
						logger.error(ex.getMessage(), ex);
					}
				}
			} else {
				runningCmd.setExecutor(this);
				try {
					runningCmd.execute();
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}
				synchronized(this) {
					runningCmd = null;
				}
			}			
		}
		return 0;
	}


}
