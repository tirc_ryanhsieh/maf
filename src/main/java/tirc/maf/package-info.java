/**
 * 
 * Message Application Framework (MAF).
 * <p>
 * Develop client/server applications with asynchronous message communication.
 * <p>
 * features:
 * <ol>
 * <li>Multi-Clients support
 * <li>QueuedCommand with priority setting
 * <li>DirectCommand
 * <li>Simple Heartbeat
 * <li>Unicast/Broadcast communication
 * <li>Async message communication
 * <li>Server/Client Application Framework
 * </ol>
 * 
 * <pre>
 * Example:
 *  1. Create a MAFServer
 *  MAFServer sever = new MAFServer();
 *  server.setCommandPackage("tirc.maf.test.cmd"); 
 *  server.setListener(new IMAFServerListener() { 
 *    public void onClientConnected(String clientId) {}
 *    public void onClinetHeartbeatTimeout(String clientId) {}
 *    public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
 *        //return a message as acknowledgement
 *        MAFMessage out = new MAFMessage("MOTOR.RUN,ACCEPT");
 *        return out; 
 *    }			
 *  });
 *  server.start(); 
 *  
 *  2. Create a MAFClient
 *  MAFClient client = new MAFClient();
 *  client.setServerAddress("127.0.0.1");
 *  client.setListener(new IMAFClientListener() {
 *     public void onServiceMessageReceived(MAFMessage msg) {
 *         // when received a unicast message from server
 *         String szmsg = msg.getMessage();
 *         switch(szmsg) {
 *         case "MOTOR.RUN,ACCEPT":
 *             // do something 
 *             break;
 *         case "MOTOR.RUN,DONE":
 *             // do something 
 *             break;
 *         }
 *     }
 *     public void onBroadcastMessageReceived(MAFMessage msg) {
 *         // when received a broadcast message from server
 *         String szmsg = msg.getMessage();
 *         switch(szmsg) {
 *         case "MOTOR.RUN,MOVING":
  *           // do something 
*             break;
 *         }   
 *     }
 *  });
 *  client.start();
 *  client.send("MOTOR.RUN,300,10");  // send "MOTOR.RUN,300,10" to server
 *  
 *  3. Implement a sample command class(MOTOR_RUN.java) in classpath(tirc/maf/test/cmd)
 *  public class MOTOR_RUN extends DirectCommand {
 *      public void execute() {		
 *         MAFMessage msg = this.getOriginalMessage();	
 *         String cmd_str = msg.getCommand();  //cmd is "MOTOR.RUN"
 *         String params_str = msg.getParamString();  //parameter is "300,10"
 *         this.broadcast("MOTOR.RUN,MOVING");  //broadcast message to all clients.
 *         this.send("MOTOR.RUN,DONE"); //send message to source client.
 *     }
 *  }
 * </pre>
 * 
 */
package tirc.maf;

