package tirc.maf;

/**
 * This abstract class is responsible for running a MAFCommand.
 * 
 * @author ryan_hsieh
 * @see MAFServer
 * @see MAFCommand
 * @see MAFMessage
 * @since 0.1
 *
 */
public abstract class MAFCommandExecutor {
	
	private MAFServer server = null;
		
	/**
	 * Construct MAFCommandExecutor with a MAFServer instance.
	 * 
	 * @param server an server instance.
	 */
	public MAFCommandExecutor(MAFServer server) {
		this.server = server;		
	}
		
	public MAFServer getMessageService() {
		return this.server;
	}
		
	/**
	 * Send message to client.
	 * 
	 * @param data a message object
	 */
	public void send(MAFMessage data) {		
		if (null != server) {
			server.send(data);
		}
	}
	
	/**
	 * This abstract function represents how to start this command executor.
	 */
	public abstract void start();
	/**
	 * This abstract function represents how to terminate this command executor.
	 */
	public abstract void terminate();
	/**
	 * This abstract function represents how this command executor executes a command. 
	 *  
	 * @param command a command object
	 * @return ture if a command is executed successfully, false otherwise.
	 */
	public abstract boolean execute(MAFCommand command);
	/**
	 * This abstract function represents if this command executor accepts or rejects a command. 
	 *  
	 * @param command a command object.
	 * @return ture if a command is accepted, false otherwise.
	 */	
	public abstract boolean accept(MAFCommand command);
	/**
	 * This abstract function represents if a executing command wants to process an incoming message. 
	 *  
	 * @param message a message object.
	 * @return true if an executing command decalres this message doesn't need to be processed further, else otherwise.
	 */	
	public abstract boolean processMessage(MAFMessage message);
	
}
