package tirc.maf;

/**
 * Default implementation of IMAFCommandExecutorPool.
 *
 * This class implements IMAFCommandExecutorPool interface. It contains two kinds of executors.
 * <ol>
 * <li><b>QueuedCommandExecutor</b><br>
 * This executor is responsible for executing QueuedCommand. All QueuedCommands will put in a queue. 
 * A QueueCommand with higher priority(0:high 255:low) will move to the head of queue. 
 * If two QueueCommand with the same priority, the order will follow FIFO sequence.
 * <li><b>DirectCommandExecutor</b><br>
 * This executor is responsible for executing DirectCommand. 
 * If a DirectCommand is executing, another DirectCommand will be rejected. 
 * </ol>   
 * 
 * @author ryan_hsieh
 * @since 0.1
 * @see IMAFCommandExecutorPool
 * @see QueuedCommandExecutor
 * @see DirectCommandExecutor
 * @see QueuedCommand
 * @see DirectCommand
 * @see MAFServer
 */
public class MAFCommandExecutorPool implements IMAFCommandExecutorPool{
	
	private QueuedCommandExecutor queuedCmdExecutor = null;
	private DirectCommandExecutor directCmdExecutor = null;
	
	public MAFCommandExecutorPool(MAFServer msgService) {
		queuedCmdExecutor = new QueuedCommandExecutor(msgService);
		directCmdExecutor = new DirectCommandExecutor(msgService);
	}
	
	public void create() {		
		queuedCmdExecutor.start();
		directCmdExecutor.start();
	}
	public void destroy() {
		queuedCmdExecutor.terminate();
		directCmdExecutor.terminate();
	}
	
	
	public boolean accept(MAFCommand command) {
		if (command instanceof QueuedCommand) {
			return queuedCmdExecutor.accept(command);
		} else if (command instanceof DirectCommand) {
			return directCmdExecutor.accept(command);
		}
		return false;
		
	}
	public boolean dispatch(MAFCommand command) {
		if (command instanceof QueuedCommand) {
			return queuedCmdExecutor.execute(command);
		} else if (command instanceof DirectCommand) {
			return directCmdExecutor.execute(command);
		}
		return false;
	}
	
	public boolean dispatch(MAFMessage message) {
		boolean result1 = this.queuedCmdExecutor.processMessage(message);
		boolean result2 = this.directCmdExecutor.processMessage(message);
		return (result1||result2);
	}
}
