package tirc.maf;

/**
 * This inteface can receive client's notifications.
 * 
 * <p>
 * A class implementing this interface can be set as the MAFClient listener.   
 * 
 * @see MAFClient
 * @author ryan_hsieh
 * @since 0.1
 */
public interface IMAFClientListener {

	/**
	 * Receive an unicast message from server.
	 * <p>
	 * This message shall have the same client identity with the current client.
	 *    
	 * @param msg an received message
	 */
	void onServiceMessageReceived(MAFMessage msg);
	
	/**
	 * Receive an broadcast message from server.
	 * <p>
	 * This message shall have no client identity.
	 *  
	 * @param msg an received message
	 */
	void onBroadcastMessageReceived(MAFMessage msg);

}
