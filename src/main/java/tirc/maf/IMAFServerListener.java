package tirc.maf;

/**
 * This inteface can receive server's notifications.
 * 
 * <p>
 * A class implementing this interface can be set as the MAFServer listener.   
 * 
 * @see MAFServer
 * @author ryan_hsieh
 * @since 0.1
 */
public interface IMAFServerListener {
	
	/**
	 * Notification for a client connected.
	 * <p>
	 * This function will be triggered when a client send a "bonjour" message.
	 * 
	 * @param clientId the identity of the connected client. 
	 */
	public void onClientConnected(String clientId);
	/**
	 * Notification for a client happened heartbeat timeout.
	 * <p>
	 * This function will be triggered when a client didn't send a "heartbeat" message within the specified period of hearbeat timeout.
	 *   
	 * @param clientId the identity of the client which happened heartbeat timeout.
	 */
	public void onClinetHeartbeatTimeout(String clientId);
	/**
	 * Notification for the status of an incoming message.
	 * <p>
	 * This function will be triggerd when an incoming message is accepted or rejected by server. 
	 * 
	 * @param inMsg an incoming message.
	 * @param result accept or reject this message.
	 * @return an output message will sent back to the source client.  
	 * @see MAFCommandReceivedResult
	 */
	public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result);

}
