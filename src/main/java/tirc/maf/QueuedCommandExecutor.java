package tirc.maf;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for executing QueuedCommand.
 * <p>
 * All QueuedCommands will put in a queue. A QueuedCommand with higher prioirty will be executed first. 
 * If QueuedCommands with the same priority will follow the FIFO sequence.
 * 
 * @since 0.1
 * @author ryan_hsieh
 * @see MAFCommandExecutor
 * @see QueuedCommand
 * 
 */
public class QueuedCommandExecutor extends MAFCommandExecutor {
	
	private static Logger logger = LoggerFactory.getLogger(QueuedCommandExecutor.class) ;
	
	private ExecutorService exec;
	private Future<Integer> result;
	private boolean bExit = false;
	private PriorityBlockingQueue<FIFOEntry<QueuedCommand>> queue = new PriorityBlockingQueue<FIFOEntry<QueuedCommand>>();
	private QueuedCommand runningCmd = null;
	
	private void offerQueue(QueuedCommand command) {
		queue.offer(new FIFOEntry<QueuedCommand>(command));		
	}
	
	private QueuedCommand pollQueue() {
		FIFOEntry<QueuedCommand> item = queue.poll();
		if (null!=item) return item.getEntry();
		return null;
	}
	
	public QueuedCommandExecutor(MAFServer msgService) {
		super(msgService);
	}
	@Override
	public boolean processMessage(MAFMessage message) {
		synchronized(this) {
			if (null != runningCmd) {
				return runningCmd.processMessage(message);
			}
		}
		return false;
	}	
	@Override
	public boolean accept(MAFCommand command) {
		return true;
	}
	@Override
	public boolean execute(MAFCommand command) {		
		offerQueue((QueuedCommand)command);
		synchronized(this) {
			notify();
		}
		return true;
	}

	@Override
	public void start() {
		exec = Executors.newSingleThreadExecutor();
		Callable<Integer> task = () -> { return main_loop(); };	
		result = exec.submit(task);

	}

	@Override
	public void terminate() {
		bExit = true;
		synchronized(this) {
			notify();		
		}
		try {
			result.get(5, TimeUnit.SECONDS);
			exec.shutdown();
			exec.awaitTermination(5, TimeUnit.SECONDS);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		finally {
		    if (!exec.isTerminated()) {		       
		    	exec.shutdownNow();
		    }
		}		
	}
	
	private int main_loop() {	
		while (!bExit) {
			QueuedCommand cmd = pollQueue(); //queue.poll();
			if (null != cmd) {
				synchronized(this) {
					runningCmd = cmd;
					cmd.setExecutor(this);
				}
				try {					
					cmd.execute();
				} catch (Exception ex) {
					logger.error(ex.getMessage(), ex);
				}
				synchronized(this) {
					runningCmd = null;
				}
			} else {
				try {
					synchronized(this) {
						wait();
					}
				} catch (InterruptedException ex) {
					logger.error(ex.getMessage(), ex);
				}
			}
		}
		return 0;
	}
}

class FIFOEntry<E extends Comparable<? super E>> implements Comparable<FIFOEntry<E>> {
	static final AtomicLong seq = new AtomicLong(0);
	final long seqNum;
	final E entry;

	public FIFOEntry(E entry) {
		seqNum = seq.getAndIncrement();
		this.entry = entry;
	}

	public E getEntry() {
		return entry;
	}

	public int compareTo(FIFOEntry<E> other) {
		int res = entry.compareTo(other.entry);
		if (res == 0 && other.entry != this.entry)
			res = (seqNum < other.seqNum ? -1 : 1);
		return res;
	}
}