package tirc.maf.test;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;


/**
 * 
 * Testcases for broadcasting messages
 * 
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BroadcastTest.class)
public class BroadcastTest {
			
	private static Logger logger = LoggerFactory.getLogger(BroadcastTest.class) ;
	
	private ZMQ.Context context = ZMQ.context(1);
	private Random rand = new Random(System.nanoTime());		
	
	private MAFServer mservice = null;
	private MAFClient client1 = null;
	private MAFClient client2 = null;
	private MAFClient client3 = null;
	
	@BeforeClass
	public static void beforeClass() {
	}
	@AfterClass
	public static void afterClass() {
	}	
	@Before
	public void setUp() throws Exception {
		mservice = createService(-1,-1,-1);		
		client1 = createClient(mservice, 19000);		
		client2 = createClient(mservice, 19001);		
		client3 = createClient(mservice, 19002);		
	}
	@After
	public void shutDown() throws Exception {
		if (null!=mservice) mservice.terminate();
		if (null!=client1) client1.terminate();
		if (null!=client2) client2.terminate();
		if (null!=client3) client3.terminate();
		mservice = null;
		client1 = null;
		client2 = null;
		client3 = null;
	}		
		
	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");			
		return mservice;
	}

	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		if (null!=service) {
			client.setServicePort(service.getServicePort());
			client.setBroadcastPort(service.getBroadcastPort());
		}
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	
		
	/**
	 *  Broadcase a message form server to all clients.
	 *  <p>
	 *  broadcast a message from the executing task. all clients shall receive this broadcast message.
	 *  <ol> 
	 *  <li>create 3 clients.
	 *  <li>one client send "QUEUE.BROADCAST"
	 *  <li>"QUEUE.BROADCAST" will broadcast a message "I'M HERE";
	 *  <li>"QUEUE.BROADCAST" will return a message "DONE";
	 *  <li>all clients will receive the "I'M HERE" broadcast message.
	 *  <li>one client will receive the "DONE" service message. 
	 *  </ol>
	 */
	@Test(timeout = 10000)
	public void broadcastTest() throws Exception {	
		
		AtomicInteger broadcastCount = new AtomicInteger();
		AtomicInteger recvCount = new AtomicInteger();
		
		IMAFClientListener clientListener = new IMAFClientListener() {			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug("recv {}:{}",msg.getClientId(), msg.getMessage());
				Assert.assertEquals(msg.getMessage(), "DONE");	
				recvCount.incrementAndGet();
			}
			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				logger.debug("broadcast:{}",msg.getMessage());
				Assert.assertEquals(msg.getMessage(), "I'M HERE");	
				broadcastCount.incrementAndGet();
			}			
		};
		
		client1.setListener(clientListener);
		client2.setListener(clientListener);
		client3.setListener(clientListener);
			
		client1.send("QUEUE.BROADCAST");

		while (1!=recvCount.get() || 3!=broadcastCount.get()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}
		
		TimeUnit.MILLISECONDS.sleep(1000);
	}

}