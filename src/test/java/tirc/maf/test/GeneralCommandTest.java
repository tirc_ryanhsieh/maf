package tirc.maf.test;

import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.IMAFServerListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFCommandReceivedResult;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;


/**
 * Testcases for basic commands.
 * 
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = GeneralCommandTest.class)
public class GeneralCommandTest {
			
	private static Logger logger = LoggerFactory.getLogger(GeneralCommandTest.class) ;
	
	private ZMQ.Context context = ZMQ.context(1);
	private Random rand = new Random(System.nanoTime());		
	
	private MAFServer mservice = null;
	private MAFClient client = null;
	
	@BeforeClass
	public static void beforeClass() {
	}
	@AfterClass
	public static void afterClass() {
	}	
	@Before
	public void setUp() throws Exception {
		mservice = createService(-1,-1,-1);		
		client = createClient(mservice, -1);		
	}
	@After
	public void shutDown() throws Exception {
		if (null!=mservice) mservice.terminate();
		if (null!=client) client.terminate();
		mservice = null;
		client = null;
	}		
		
	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");			
		return mservice;
	}

	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		client.setServicePort(service.getServicePort());
		client.setBroadcastPort(service.getBroadcastPort());
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	
		
	/**
	 *  DirectCommand normal test.
	 *  <p>steps:
	 *  <ol>
	 *  <li>client send a direct command. e.g."DIRECT.ADD,1,2"
	 *  <li>service return the command ack. e.g."DIRECT.ADD,ACK,ACCEPT"
	 *  <li>service execute the command.
	 *  <li>service return the command result. e.g."DIRECT.ADD,RET,0,3"
	 *  </ol>
	 * 
	 */
	@Test(timeout = 5000)
	public void directCmdFlowTest() throws Exception {	
		LinkedBlockingQueue<MAFMessage> msgQueue = new LinkedBlockingQueue<MAFMessage>();		
				
		mservice.setListener(new IMAFServerListener() {

			@Override
			public void onClientConnected(String clientId) {}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				System.out.println(inMsg.getMessage());
				MAFMessage outMsg = new MAFMessage();
				outMsg.setMessage(inMsg.getCommand()+",ACK,"+result.name());				
				return outMsg;
			}			
		});
		
		client.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				msgQueue.offer(msg);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
						
		int a1 = rand.nextInt(100);
		int a2 = rand.nextInt(100);
		
		String cmd = String.format("DIRECT.ADD,%d,%d",a1,a2);
		logger.debug("sent: {}", cmd);
		client.send(cmd);

		MAFMessage ack = msgQueue.take();
		Assert.assertNotNull(ack);
		logger.debug("ack: {}", ack.getMessage());
		Assert.assertEquals("DIRECT.ADD,ACK,ACCEPT", ack.getMessage());
		
		MAFMessage ret = msgQueue.take();
		Assert.assertNotNull(ret);
		logger.debug("return: {}", ret.getMessage());
		Assert.assertTrue(ret.getMessage().startsWith("DIRECT.ADD,RET"));
		String[] params = ret.getParamString().split(",");		
		Assert.assertTrue(Integer.valueOf(params[2])==(a1+a2));
		
	}

	/**
	 *  DirectCommand failure test
	 *  <p>if a direct command is executing, another direct command shall be rejected.
	 *  <p>step:
	 *  <ol>
	 *  <li>client send a direct command to sleep 10seconds. e.g."DIRECT.SLEEP,10"
	 *  <li>client send another direct command. e.g."DIRECT.ADD,1,2"
	 *  <li>the second command shall be rejected. e.g. "DIRECT.ADD,ACK,BUSY"
	 *  </ol>
	 */
	@Test(timeout = 5000)
	public void directCmdBusyTest() throws Exception {
		LinkedBlockingQueue<MAFMessage> msgQueue = new LinkedBlockingQueue<MAFMessage>();

			mservice.setListener(new IMAFServerListener() {
				@Override
				public void onClientConnected(String clientId) {
				}

				@Override
				public void onClinetHeartbeatTimeout(String clientId) {
				}

				@Override
				public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
					MAFMessage outMsg = new MAFMessage();
					outMsg.setMessage(inMsg.getCommand() + ",ACK," + result.name());
					return outMsg;
				}
			});

			client.setListener(new IMAFClientListener() {
				@Override
				public void onServiceMessageReceived(MAFMessage msg) {
					msgQueue.offer(msg);
				}

				@Override
				public void onBroadcastMessageReceived(MAFMessage msg) {
					Assert.assertTrue(false);
				}
			});

			String cmd = String.format("DIRECT.SLEEP,3");
			logger.debug("sent: {}", cmd);
			client.send(cmd);

			MAFMessage ack = msgQueue.take();
			Assert.assertNotNull(ack);
			logger.debug("ack: {}", ack.getMessage());
			Assert.assertEquals("DIRECT.SLEEP,ACK,ACCEPT", ack.getMessage());

			int a1 = rand.nextInt(100);
			int a2 = rand.nextInt(100);

			cmd = String.format("DIRECT.ADD,%d,%d", a1, a2);
			logger.debug("sent: {}", cmd);
			client.send(cmd);

			ack = msgQueue.take();
			Assert.assertNotNull(ack);
			logger.debug("ack: {}", ack.getMessage());
			Assert.assertEquals("DIRECT.ADD,ACK,BUSY", ack.getMessage());
		
	}
	
	/**
	 *  QueuedCommand test.
	 *  <p>steps:
	 *  <ol>
	 *  <li>client send a queued command. e.g."QUEUE.ADD,1,2"
	 *  <li>service return the command ack. e.g."QUEUE.ADD,ACK,ACCEPT"
	 *  <li>service execute the command.
	 *  <li>service return the command result. e.g."QUEUE.ADD,RET,0,3"
	 *  </ol>
	 */
	@Test(timeout = 5000)
	public void queuedCmdFlowTest() throws Exception {	
		LinkedBlockingQueue<MAFMessage> msgQueue = new LinkedBlockingQueue<MAFMessage>();		
		mservice.setListener(new IMAFServerListener() {

			@Override
			public void onClientConnected(String clientId) {}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				MAFMessage outMsg = new MAFMessage();
				outMsg.setMessage(inMsg.getCommand()+",ACK,"+result.name());				
				return outMsg;
			}			
		});
		

		client.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				msgQueue.offer(msg);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
						
		int a1 = rand.nextInt(100);
		int a2 = rand.nextInt(100);
		
		String cmd = String.format("QUEUE.ADD,%d,%d",a1,a2);
		logger.debug("sent: {}", cmd);
		client.send(cmd);

		MAFMessage ack = msgQueue.take();
		Assert.assertNotNull(ack);
		logger.debug("ack: {}", ack.getMessage());
		Assert.assertEquals("QUEUE.ADD,ACK,ACCEPT",ack.getMessage());
		
		MAFMessage ret = msgQueue.take();
		Assert.assertNotNull(ret);
		logger.debug("return: {}", ret.getMessage());
		Assert.assertTrue(ret.getMessage().startsWith("QUEUE.ADD,RET"));
		String[] params = ret.getParamString().split(",");		
		Assert.assertTrue(Integer.valueOf(params[2])==(a1+a2));
		
		
	}
	
	/**
	 *  Multiple QueuedCommand test.
	 *  <p>
	 *  send multiple queued commands to servers, check whether all returns can be received in sequence
	 *  <ol> 
	 *  <li>client sent "QUEUE_CMD.SLEEP,2"
	 *  <li>client send "QUEUE.ADD,1,2", "QUEUE.ADD,3,5", "QUEUE.ADD,6,10", "QUEUE.ADD,11,12" 
	 *  <li>check the results in sequence from server. "QUEUE.ADD,RET,0,3", "QUEUE.ADD,RET,0,8", "QUEUE.ADD,RET,0,16", "QUEUE.ADD,RET,0,23" 
	 *  </ol>
	 */
	@Test(timeout = 5000)
	public void multiQueuedCmdTest() throws Exception {	
		AtomicBoolean bExit = new AtomicBoolean(false);

		client.setListener(new IMAFClientListener() {
			private int i=0;
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug(msg.getMessage());
				switch(i) {
				case 0:
					Assert.assertTrue(0==msg.getMessage().compareTo("QUEUE.ADD,RET,0,3"));
					break;
				case 1:
					Assert.assertTrue(0==msg.getMessage().compareTo("QUEUE.ADD,RET,0,8"));
					break;
				case 2:
					Assert.assertTrue(0==msg.getMessage().compareTo("QUEUE.ADD,RET,0,16"));
					break;
				case 3:
					Assert.assertTrue(0==msg.getMessage().compareTo("QUEUE.ADD,RET,0,23"));
					break;					
				}
				i++;
				if (4==i) bExit.set(true);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
								
		String cmd = String.format("QUEUE_CMD.SLEEP,2");
		logger.debug("sent: {}", cmd);
		client.send(cmd);
		
		cmd = String.format("QUEUE.ADD,1,2");
		logger.debug("sent: {}", cmd);
		client.send(cmd);
		
		cmd = String.format("QUEUE.ADD,3,5");
		logger.debug("sent: {}", cmd);
		client.send(cmd);

		cmd = String.format("QUEUE.ADD,6,10");
		logger.debug("sent: {}", cmd);
		client.send(cmd);

		cmd = String.format("QUEUE.ADD,11,12");
		logger.debug("sent: {}", cmd);
		client.send(cmd);
		
		while(!bExit.get()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}		
	}

	/**
	 *  Test for a message with attachments. 
	 *  <p>send/receive a message with attachments.
	 *  <ol>
	 *  <li>create a MAFMessage with attachments and message "QUEUE.ECHO".   
	 *  <li>client sent this MAFMessage
	 *  <li>servier return the same MAFMessage 
	 *  <li>check the content of MAFMessage.
	 *  </ol>  
	 */
	@Test(timeout = 5000)
	public void attachmentTransmitTest() throws Exception {	
		AtomicBoolean bExit = new AtomicBoolean(false);

		byte[] dummy = new byte[512];
		for (int i=0; i< dummy.length; i++) {
			dummy[i] = 0x0F;
		}
		
		client.setListener(new IMAFClientListener() {
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug("recv {}:{}", msg.getClientId(), msg.getMessage());
				Assert.assertEquals("QUEUE.ECHO", msg.getMessage());
				Assert.assertEquals(2, msg.getAttachmentNum());
				byte[] p1 = msg.getAttachment(0);
				byte[] p2 = msg.getAttachment(1);
				Assert.assertEquals("hello", new String(p1));
				Assert.assertArrayEquals(p2, dummy);
				
				bExit.set(true);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
			
		
		MAFMessage msg = new MAFMessage();
		msg.setMessage("QUEUE.ECHO");
		msg.putAttachment("hello".getBytes());
		msg.putAttachment(dummy);
		client.send(msg);
				
		while(!bExit.get()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}	
		
		TimeUnit.MILLISECONDS.sleep(1000);
	}
	

}