package tirc.maf.test;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.IMAFServerListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFCommandReceivedResult;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;

/**
 * Testacase for the special command "heartbeat". 
 * 
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = HeartbeatCommandTest.class)

public class HeartbeatCommandTest {
			
	private static Logger logger = LoggerFactory.getLogger(HeartbeatCommandTest.class) ;
	
	private ZMQ.Context context = ZMQ.context(1);
	private Random rand = new Random(System.nanoTime());		
	
	private MAFServer mservice = null;
	private MAFClient client = null;
	
	@BeforeClass
	public static void beforeClass() {
	}
	@AfterClass
	public static void afterClass() {
	}	
	@Before
	public void setUp() throws Exception {
		mservice = createService(-1,-1,-1);		
		client = createClient(mservice, -1);		
	}
	@After
	public void shutDown() throws Exception {
		if (null!=mservice) mservice.terminate();
		if (null!=client) client.terminate();
		mservice = null;
		client = null;
	}		
		
	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");			
		return mservice;
	}

	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		if (null!=service) {
			client.setServicePort(service.getServicePort());
			client.setBroadcastPort(service.getBroadcastPort());
		}
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	
		
	/**
	 *  Heartbeat normal test
	 *  <ol>
	 *  <li>enable server's heartbeat feature. 
	 *  <li>client regularly send the heartbeat command to avoid heartbeat timeout.
	 *  </ol>
	 */
	@Test(timeout = 10000)
	public void heartbeatCmdTest() throws Exception {	
		
		mservice.setHeartbeatInterval(1000);
		mservice.setListener(new IMAFServerListener() {
			@Override
			public void onClientConnected(String clientId) {
				Assert.assertTrue(false);
			}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {
				Assert.assertTrue(false);
			}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				Assert.assertTrue(false);
				return null;
			}			
		});
		
		client.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
								
		for (int i=0;i<10;i++) {
			logger.debug("send heartbeat({})",i);
			client.send("heartbeat");
			TimeUnit.MILLISECONDS.sleep(800);
		}
		mservice.setHeartbeatInterval(0);
		TimeUnit.MILLISECONDS.sleep(1000);
	}

	/**
	 *  Heartbeat failure test
	 *  <p>
	 *  steps:
	 *  <ol>
	 *  <li>enable server's heartbeat feature. 
	 *  <li>client does not send any heartbeat command.
	 *  <li>server shall trigger a heartbear timeout and inovke IMAFServerListener.onClinetHeartbeatTimeout();
	 *  </ol>
	 *  
 	 * @throws Exception
	 */
	@Test(timeout = 5000)
	public void heartbeatTimeoutTest() throws Exception {	
		
		AtomicBoolean clientTimeout = new AtomicBoolean(false);
				
		mservice.setHeartbeatInterval(1000);
		mservice.setListener(new IMAFServerListener() {
			@Override
			public void onClientConnected(String clientId) {
				Assert.assertTrue(false);
			}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {
				logger.debug("client heartbeat timeout:{}", clientId);
				clientTimeout.set(true);
			}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				Assert.assertTrue(false);
				return null;
			}			
		});
		
		client.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
		
		client.send("heartbeat"); //send one heartbeat at least to let server to know the client.

		while (!clientTimeout.get()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}

		mservice.setHeartbeatInterval(0);
		TimeUnit.MILLISECONDS.sleep(1000);
	}

}