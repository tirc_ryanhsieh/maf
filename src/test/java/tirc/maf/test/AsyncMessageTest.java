package tirc.maf.test;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;


/**
 * 
 * Testcases for exchanging message in the same command.
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AsyncMessageTest.class)
public class AsyncMessageTest {
			
	private static Logger logger = LoggerFactory.getLogger(AsyncMessageTest.class) ;
	
	private ZMQ.Context context = ZMQ.context(1);
	private Random rand = new Random(System.nanoTime());		
	
	private MAFServer server = null;
	private MAFClient client = null;
	
	@BeforeClass
	public static void beforeClass() {
	}
	@AfterClass
	public static void afterClass() {
	}	
	@Before
	public void setUp() throws Exception {
		server = createService(-1,-1,-1);		
		client = createClient(server, 19000);		
	}
	@After
	public void shutDown() throws Exception {
		if (null!=server) server.terminate();
		if (null!=client) client.terminate();
		server = null;
		client = null;
	}		
		
	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");			
		return mservice;
	}

	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		if (null!=service) {
			client.setServicePort(service.getServicePort());
			client.setBroadcastPort(service.getBroadcastPort());
		}
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	
		
	/**
	 *  Exchange message in the same direct command.
	 *  <p>
	 *  process messages in the same MAFCommand.
	 *  <p>
	 *  steps:
	 *  <ol> 
	 *  <li>client send "DIRECT_ASYNC" to create a MAFCommand.
	 *  <li>client send "1" to the "DIRECT_AYSNC" command.
	 *  <li>the "DIRECT_ASYNC" command send "2" to client.
	 *  <li>client send "3" to the "DIRECT_ASYNC" command.
	 *  <li>the "DIRECT_ASYNC" command send "4" to client.
	 *  <li>client send "5" to the "DIRECT_ASYNC" command.
	 *  <li>"DIRECT_ASYNC" exit and send "6" to client.
	 *  <li>client receive "6" to exit 
	 *  </ol>
	 */
	@Test(timeout = 10000)
	public void directAsyncTest() throws Exception {	
		
		AtomicInteger stage = new AtomicInteger(0);
		
		IMAFClientListener clientListener = new IMAFClientListener() {			
			@Override
			public void onServiceMessageReceived(MAFMessage message) {
				String msg = message.getMessage();
				Assert.assertEquals(stage.get()+1, Integer.valueOf(msg).intValue());
				stage.set(Integer.valueOf(msg));				
			}
			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);	
			}			
		};		
		client.setListener(clientListener);
		
		client.send("DIRECT.ASYNC");
		
		TimeUnit.MILLISECONDS.sleep(500);
		
		boolean bExit = false;
		
		while (!bExit) {
			switch (stage.get()) {
			case 0:
				logger.debug("step 0");
				client.send("1");
				stage.set(1);
				break;
			case 2:
				logger.debug("step 2");
				client.send("3");
				stage.set(3);
				break;
			case 4:
				logger.debug("step 4");
				client.send("5");
				stage.set(5);
				break;
			case 6:
				logger.debug("step 6");
				bExit = true;;
				break;
			}			
			try {
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

	
	/**
	 *  Exchange message in the same direct command.
	 *  <p> 
	 *  process messages in the same MAFCommand.
	 *  <p>
	 *  steps:
	 *  <ol>
	 *  <li>client send "QUEUE_ASYNC" to create a MAFCommand.
	 *  <li>client send "1" to the "QUEUE_ASYNC" command.
	 *  <li>the "QUEUE_ASYNC" command send "2" to client.
	 *  <li>client send "3" to the "QUEUE_ASYNC" command.
	 *  <li>the "DIRECT_ASYNC" command send "4" to client.
	 *  <li>client send "5" to the "QUEUE_ASYNC" command.
	 *  <li>"QUEUE_ASYNC" exit and send "6" to client.
	 *  <li>client receive "6" to exit 
	 *  </ol>
	 */
	@Test(timeout = 10000)
	public void queueAsyncTest() throws Exception {	
		
		AtomicInteger stage = new AtomicInteger(0);
		
		IMAFClientListener clientListener = new IMAFClientListener() {			
			@Override
			public void onServiceMessageReceived(MAFMessage message) {
				String msg = message.getMessage();
				Assert.assertEquals(stage.get()+1, Integer.valueOf(msg).intValue());
				stage.set(Integer.valueOf(msg));				
			}
			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);	
			}			
		};		
		client.setListener(clientListener);
		
		client.send("QUEUE.ASYNC");
		
		TimeUnit.MILLISECONDS.sleep(500);
		
		boolean bExit = false;
		
		while (!bExit) {
			switch (stage.get()) {
			case 0:
				logger.debug("step 0");
				client.send("1");
				stage.set(1);
				break;
			case 2:
				logger.debug("step 2");
				client.send("3");
				stage.set(3);
				break;
			case 4:
				logger.debug("step 4");
				client.send("5");
				stage.set(5);
				break;
			case 6:
				logger.debug("step 6");
				bExit = true;;
				break;
			}			
			try {
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
}