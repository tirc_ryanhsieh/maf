package tirc.maf.test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;

/**
 * Testcases for queue commands with priority setting.
 * 
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = QueuedCommandPriorityTest.class)

public class QueuedCommandPriorityTest {
			
	private ZMQ.Context context = ZMQ.context(1);
			
	@BeforeClass
	public static void beforeClass() {
	}
	@AfterClass
	public static void afterClass() {
	}	
	@Before
	public void setUp() throws Exception {
	}
	@After
	public void shutDown() throws Exception {
	}		
		
	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");			
		return mservice;
	}

	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		client.setServicePort(service.getServicePort());
		client.setBroadcastPort(service.getBroadcastPort());
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	
	class MAFClientListener implements IMAFClientListener {
		
		private MAFClient client;
		private int cmdCount;
		private int prevPriority = -1;
		
		public MAFClientListener(MAFClient client, int count) {
			this.client = client;
			this.cmdCount = count;
		}
		public int getCmdCount() {
			return cmdCount;
		}
		
		@Override
		public void onServiceMessageReceived(MAFMessage msg) {
      		int priority = Integer.valueOf(msg.getMessage());
      		cmdCount--;
      		System.out.println(client.getClientId()+":"+priority+","+prevPriority+","+cmdCount);	      		
      		Assert.assertTrue(priority>=prevPriority);
      		prevPriority = priority;
		}

		@Override
		public void onBroadcastMessageReceived(MAFMessage msg) {
			Assert.assertTrue(false);			
		}		
	}
		
	/**
	 * Priority QueuedCommand test
	 * <p>
	 * random 250 queued commands with different priorities,
	 * <br> 
	 * expect the executing order of commands by the priority form high(0) to low(255)
	 */
	@Test(timeout = 10000)
	public void priorityQueueTest() throws Exception {	
		final int TEST_CMD_NUM = 250;
		
		MAFServer mservice = createService(-1,-1,-1);

		MAFClient client = createClient(mservice, -1);
		MAFClientListener client_listener = new MAFClientListener(client, TEST_CMD_NUM);
		client.setListener(client_listener);

		client.send("QUEUE_CMD_SLEEP,2");
		
		for (int i=0; i<TEST_CMD_NUM; i++) {
			client.send("QUEUE_CMD_RAND_PRIORITY");
		}
		
		//wait the clients to stop		
		while (true) {
			try {
				client.getResult().get(100, TimeUnit.MILLISECONDS);
			} catch (TimeoutException e) {
			}
			if (0==client_listener.getCmdCount()) break;
		}
		client.terminate();
		mservice.terminate();
		
	}

	/**
	 * Priority QueuedCommand test
	 * <p>
	 * create two services and send random 250 queued commands with different priorities,
	 * <br> 
	 * expect the executing order of commands by the priority form high(0) to low(255)
	 *  
	 */
	@Repeat(3)
	@Test(timeout = 30000)
	public void multipleServiceTest() throws Exception {
		final int TEST_CMD_NUM = 250;
		
		MAFServer mservice1 = createService(15000,15001,15002);
		MAFServer mservice2 = createService(16000,16001,16002);
		
		MAFClient client1 = createClient(mservice1, 17000);
		MAFClientListener client1_listener = new MAFClientListener(client1, TEST_CMD_NUM);
		client1.setListener(client1_listener);

		MAFClient client2 = createClient(mservice2, 17001);
		MAFClientListener client2_listener = new MAFClientListener(client2, TEST_CMD_NUM);
		client2.setListener(client2_listener);
							
		client1.send("QUEUE_CMD_SLEEP,2");
		client2.send("QUEUE_CMD_SLEEP,2");
		
		TimeUnit.MILLISECONDS.sleep(100);  // make sure sleep command executed in advance
		
		for (int i=0; i<TEST_CMD_NUM; i++) {
			client1.send("QUEUE_CMD_RAND_PRIORITY");
			client2.send("QUEUE_CMD_RAND_PRIORITY");
		}
		
		//wait the clients to stop					
		while (true) {
			try {
				client1.getResult().get(10, TimeUnit.MILLISECONDS);
				client2.getResult().get(10, TimeUnit.MILLISECONDS);
			} catch (TimeoutException e) {
			}
			if (0==client1_listener.getCmdCount() && 0==client2_listener.getCmdCount()) break;
		}
		client1.terminate();
		client2.terminate();
				
		mservice1.terminate();
		mservice2.terminate();
		
	}
	

}