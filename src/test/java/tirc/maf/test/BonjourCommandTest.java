package tirc.maf.test;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.IMAFServerListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFCommandReceivedResult;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;


/**
 * Testacase for the special command "bonjour". 
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BonjourCommandTest.class)
public class BonjourCommandTest {
			
	private static Logger logger = LoggerFactory.getLogger(BonjourCommandTest.class) ;
	
	private ZMQ.Context context = ZMQ.context(1);
	private Random rand = new Random(System.nanoTime());		
	
	private MAFServer mservice = null;
	private MAFClient client = null;
	
	@BeforeClass
	public static void beforeClass() {
	}
	@AfterClass
	public static void afterClass() {
	}	
	@Before
	public void setUp() throws Exception {
		mservice = createService(-1,-1,-1);		
		client = createClient(mservice, -1);		
	}
	@After
	public void shutDown() throws Exception {
		if (null!=mservice) mservice.terminate();
		if (null!=client) client.terminate();
		mservice = null;
		client = null;
	}		
		
	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");			
		return mservice;
	}

	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		if (null!=service) {
			client.setServicePort(service.getServicePort());
			client.setBroadcastPort(service.getBroadcastPort());
		}
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	
		
	/**
	 *  bonjour command test
	 *  <p>steps:
	 *  <ol>
	 *  <li>client send a bonjour string.
	 *  <li>service return bonjour immediately and invoke IMAFServerListener.onClientConnected().
	 *  </ol>
	 */
	@Test(timeout = 5000)
	public void bonjourCmdTest() throws Exception {	

		AtomicBoolean clientConnected = new AtomicBoolean(false);
		AtomicBoolean bonjourReceived = new AtomicBoolean(false);
		
		mservice.setListener(new IMAFServerListener() {
			@Override
			public void onClientConnected(String clientId) {
				logger.debug("client connected:{}", clientId);
				clientConnected.set(true);
			}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {
				Assert.assertTrue(false);
			}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				Assert.assertTrue(false);
				return null;
			}			
		});
		
		client.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug("recv:{}", msg.getMessage());
				Assert.assertEquals(msg.getMessage(),"bonjour");
				bonjourReceived.set(true);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
								
		client.send("bonjour");

		while (bonjourReceived.get()==false || clientConnected.get()==false) {
			TimeUnit.MILLISECONDS.sleep(10);
		}		
	}

	/**
	 *  bonjour command test
	 *  <p>
	 *  When server is not ready, a client send a bonjour command.
	 *  The client shall get a bonjour when server is ready.
	 */
	@Test(timeout = 5000)
	public void bonjourDelayTest() throws Exception {	

		AtomicBoolean clientConnected = new AtomicBoolean(false);
		AtomicBoolean bonjourReceived = new AtomicBoolean(false);
		
		int serviceport = 36000;
		int braodcastport = 36001;
		int clientsignalport = 36002;
		int serversignalport = 36003;
	
		MAFClient client1 = new MAFClient(context);
		client1.setServicePort(serviceport);
		client1.setBroadcastPort(braodcastport);
		client1.setSignalPort(clientsignalport);
		client1.start();
		
		MAFServer server1 = new MAFServer(context);
		server1.setServicePort(serviceport);
		server1.setBroadcastPort(braodcastport);
		server1.setSignalPort(serversignalport);
		
		new Thread() {
			public void run() {
				try {
					TimeUnit.SECONDS.sleep(2);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				server1.start();
			}
		}.start();
		
		server1.setListener(new IMAFServerListener() {
			@Override
			public void onClientConnected(String clientId) {
				logger.debug("client connected:{}", clientId);
				clientConnected.set(true);
			}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {
				Assert.assertTrue(false);
			}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				Assert.assertTrue(false);
				return null;
			}			
		});
		
		client1.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug("recv:{}", msg.getMessage());
				Assert.assertEquals(msg.getMessage(),"bonjour");
				bonjourReceived.set(true);
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
								
		client1.send("bonjour");

		while (bonjourReceived.get()==false || clientConnected.get()==false) {
			TimeUnit.MILLISECONDS.sleep(10);
		}	
		client1.terminate();
		server1.terminate();
	}	

	/**
	 *  bonjour command test
	 *  <p>
	 *  Two client say bonjour to server. Each client shall receive bonjour from server. 
	 *  
	 */
	@Test(timeout = 5000)
	public void twoClientBonjourCmdTest() throws Exception {	

		AtomicInteger clientConnected = new AtomicInteger();
		AtomicInteger bonjourReceived = new AtomicInteger();
		
		MAFClient client2 = this.createClient(this.mservice, 30023);
		
		mservice.setListener(new IMAFServerListener() {
			@Override
			public void onClientConnected(String clientId) {
				logger.debug("client connected:{}", clientId);
				clientConnected.incrementAndGet();
			}

			@Override
			public void onClinetHeartbeatTimeout(String clientId) {
				Assert.assertTrue(false);
			}

			@Override
			public MAFMessage onMessageReceived(MAFMessage inMsg, MAFCommandReceivedResult result) {
				Assert.assertTrue(false);
				return null;
			}			
		});
		
		client.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug("recv:{}", msg.getMessage());
				Assert.assertEquals(msg.getMessage(),"bonjour");
				bonjourReceived.incrementAndGet();
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
		
		client2.setListener(new IMAFClientListener() {
			
			@Override
			public void onServiceMessageReceived(MAFMessage msg) {
				logger.debug("recv:{}", msg.getMessage());
				Assert.assertEquals(msg.getMessage(),"bonjour");
				bonjourReceived.incrementAndGet();
			}

			@Override
			public void onBroadcastMessageReceived(MAFMessage msg) {
				Assert.assertTrue(false);
			}
			
		});
		
								
		client.send("bonjour");
		client2.send("bonjour");

		while (bonjourReceived.get()!=2 || clientConnected.get()!=2) {
			TimeUnit.MILLISECONDS.sleep(10);
		}	
		
		client2.terminate();
	}	
}