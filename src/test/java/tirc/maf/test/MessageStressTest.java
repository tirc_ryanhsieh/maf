package tirc.maf.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.zeromq.ZMQ;

import tirc.maf.IMAFClientListener;
import tirc.maf.MAFClient;
import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;

import org.slf4j.Logger;

/**
 * 
 * Testcase for stress testing
 * 
 * @author ryan_hsieh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MessageStressTest.class)

public class MessageStressTest {
	
	private static Logger logger = LoggerFactory.getLogger(MessageStressTest.class) ;

	private static int EACH_CLIENT_SEND_COUNT = 10000;
	private static int CLIENT_NUM = 5;
	
	private MAFServer mservice;
	private ZMQ.Context context = ZMQ.context(1);
	private AtomicInteger signalPort = new AtomicInteger(30055); 
	private Random rand = new Random(System.nanoTime());
	
	
	public static String pause(String prompt)  {
		System.out.print(prompt);
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));					
		String ans = "";
		try {
			ans = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return ans;
	}	

	private MAFServer createService(int servicePort, int broadcastPort, int signalPort) {
		MAFServer mservice = new MAFServer(context);
		if (servicePort>0)	mservice.setServicePort(servicePort);
		if (broadcastPort>0) mservice.setBroadcastPort(broadcastPort);
		if (signalPort>0) mservice.setSignalPort(signalPort);
		mservice.start();
		mservice.setCommandPackage("tirc.maf.test.cmd");	
		return mservice;
	}
	private MAFClient createClient(MAFServer service, int signalPort) {
		MAFClient client = new MAFClient(context);
		client.setServerAddress("localhost");
		if (null!=service) {
			client.setServicePort(service.getServicePort());
			client.setBroadcastPort(service.getBroadcastPort());
		}
		if (signalPort>0) client.setSignalPort(signalPort);
		client.start();
		return client;
	}
	@Before
	public void setUp() throws Exception {
		mservice = createService(-1,-1,-1);	
	}
	@After
	public void shutDown() throws Exception {
		if (null!=mservice) mservice.terminate();
	}	
	/**
	 * Stress testing 
	 * <p>
	 * steps:
	 * <ol>
	 * <li>	 Each client sends a message to the server repeatedly.
	 * <li>  Server shall execute a proper command object.
	 * <li>  The command object broadcast message to all clients.
	 * </ol> 
	 * 
	 */
	@Repeat(2)
	@Test
	public void sressTest() throws Exception {
		
		ExecutorService executor = Executors.newWorkStealingPool(CLIENT_NUM);
		
		List<Callable<Long>> callables = new ArrayList<Callable<Long>>(); 
		for (int i=0;i<CLIENT_NUM;i++) {
			callables.add(() -> { return runClient();} );
		}

		List<Future<Long>> results = executor.invokeAll(callables);
		
		long totalReceived = 0;
		for (Future<Long> future: results) {
			logger.debug("received:"+future.get());
			totalReceived += future.get();
		}
		logger.debug("total received:"+totalReceived);
		
	}
	
	public long runClient() throws InterruptedException{
		final MAFClient client = createClient(mservice, signalPort.incrementAndGet());;	
		AtomicBoolean bConnected = new AtomicBoolean(false);
		AtomicLong receivedCount = new AtomicLong(0);
	 
		try {			
			client.setListener(new IMAFClientListener(){
				@Override
				public void onServiceMessageReceived(MAFMessage msg) {
					logger.debug("{}:{}", msg.getClientId() ,msg.getMessage());
					if (0==msg.getMessage().compareToIgnoreCase("bonjour"))	{						
						bConnected.set(true);
					} else if (0==msg.getMessage().compareTo("QUEUE.STRESS,finish")) {
						receivedCount.incrementAndGet();
					}
				}
				@Override
				public void onBroadcastMessageReceived(MAFMessage msg) {
					receivedCount.incrementAndGet();
					//logger.debug("{}",msg.getMessage());
				}				
			});
			
			client.send("bonjour");		
			// waiting for server ready....
			while (!bConnected.get()) {	TimeUnit.MILLISECONDS.sleep(10); 	}
			
			new Thread() {
				public void run() {
					for (int i=0; i<EACH_CLIENT_SEND_COUNT;i++) {
						//logger.debug("client send:{}",i);
						if (i==(EACH_CLIENT_SEND_COUNT-1)) {
							client.send("QUEUE.STRESS,finish");					
						} else {
							MAFMessage msg = new MAFMessage();
							msg.setMessage("QUEUE.STRESS,running,"+i);
							msg.putAttachment(new byte[rand.nextInt(1024)]); 
							client.send(msg);
						}
						try {
							TimeUnit.MILLISECONDS.sleep(rand.nextInt(10));
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
					}
					logger.debug("client({}) sent done", client.getClientId());					
				}
			}.start();
			
			int expectCount = (EACH_CLIENT_SEND_COUNT-1)*CLIENT_NUM+1;
			
			logger.debug("expect to receive:{}", expectCount);
			while (receivedCount.get() != expectCount) {
				logger.debug("client({}) recv:{}",client.getClientId(), receivedCount.get() );
				TimeUnit.MILLISECONDS.sleep(2000);
			}
			
		} finally {
			if (null!=client) client.terminate();
		}
		return receivedCount.get();		
	}

}