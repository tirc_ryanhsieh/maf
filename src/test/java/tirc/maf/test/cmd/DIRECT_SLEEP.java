package tirc.maf.test.cmd;

import java.util.concurrent.TimeUnit;

import tirc.maf.DirectCommand;

public class DIRECT_SLEEP extends DirectCommand {
	
	public DIRECT_SLEEP() {
	}
	
	@Override
	public void execute() {
		try {
			TimeUnit.SECONDS.sleep(Integer.valueOf(this.getOriginalMessage().getParamString()));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
