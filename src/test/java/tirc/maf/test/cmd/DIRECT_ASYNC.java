package tirc.maf.test.cmd;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tirc.maf.DirectCommand;
import tirc.maf.MAFMessage;

public class DIRECT_ASYNC extends DirectCommand {
		
	private static Logger logger = LoggerFactory.getLogger(DIRECT_ASYNC.class) ;
	
	public AtomicInteger stage = new AtomicInteger(0);
	
	public DIRECT_ASYNC() {
	}
	
	@Override
	public void execute() {
		boolean bExit = false;
		
		while (!bExit) {
			switch (stage.get()) {
			case 1:
				logger.debug("step 1");
				this.send("2");
				stage.set(2);
				break;
			case 3:
				logger.debug("step 3");
				this.send("4");
				stage.set(4);
				break;
			case 5:
				logger.debug("step 5");
				this.send("6");
				stage.set(6);
				bExit = true;;
				break;
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean processMessage(MAFMessage message) {
		String msg = message.getMessage();
		//logger.debug("recv msg:{}",msg);
		stage.set(Integer.valueOf(msg));
		return true;
	}
	
}
