package tirc.maf.test.cmd;

import tirc.maf.MAFMessage;
import tirc.maf.QueuedCommand;

public class QUEUE_BROADCAST extends QueuedCommand {
		
	public QUEUE_BROADCAST() {
	}
	
	@Override
	public void execute() {
		
		MAFMessage broadcastMsg = new MAFMessage();
		MAFMessage retMsg = new MAFMessage();
		
		broadcastMsg.setMessage("I'M HERE");
		retMsg.setMessage("DONE");
		
		this.broadcast(broadcastMsg);
		this.send(retMsg);
		
	}
}
