package tirc.maf.test.cmd;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tirc.maf.QueuedCommand;

public class QUEUE_CMD_SLEEP extends QueuedCommand {
	private static Logger logger = LoggerFactory.getLogger(QUEUE_CMD_SLEEP.class) ;
	
	public QUEUE_CMD_SLEEP() {
	}
	
	@Override
	public void execute() {
		logger.debug("{}:{}", this.getOriginalMessage().getClientId(),this.getOriginalMessage().getMessage());
		try {
			TimeUnit.SECONDS.sleep(Integer.valueOf(this.getOriginalMessage().getParamString()));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
