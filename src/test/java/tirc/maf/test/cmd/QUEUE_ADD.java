package tirc.maf.test.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tirc.maf.MAFMessage;
import tirc.maf.QueuedCommand;

public class QUEUE_ADD extends QueuedCommand {
	
	private static Logger logger = LoggerFactory.getLogger(QUEUE_ADD.class) ;
	
	public QUEUE_ADD() {
	}
	
	@Override
	public void execute() {
		
		logger.debug("{}:{}", this.getOriginalMessage().getClientId(),this.getOriginalMessage().getMessage());
		
		MAFMessage inMsg = this.getOriginalMessage();
		String[] params = inMsg.getParamString().split(",");
		
		MAFMessage outMsg = new MAFMessage();
		if (params.length == 2) {
			int p1 = Integer.valueOf(params[0]);
			int p2 = Integer.valueOf(params[1]);
			outMsg.setMessage(String.format("%s,RET,0,%d", this.getOriginalMessage().getCommand(),(p1+p2)));			
		} else {
			outMsg.setMessage(String.format("%s,RET,-1", this.getOriginalMessage().getCommand()));
		}	
		
		this.send(outMsg);
	}
}
