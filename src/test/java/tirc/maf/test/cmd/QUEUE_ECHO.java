package tirc.maf.test.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tirc.maf.MAFServer;
import tirc.maf.QueuedCommand;

public class QUEUE_ECHO extends QueuedCommand {
		
	private static Logger logger = LoggerFactory.getLogger(QUEUE_ECHO.class) ;
	
	public QUEUE_ECHO() {
	}
	
	@Override
	public void execute() {
				
		logger.debug("{}:{}", this.getOriginalMessage().getClientId(),this.getOriginalMessage().getMessage());
			
		this.send(this.getOriginalMessage());
		

	}
}
