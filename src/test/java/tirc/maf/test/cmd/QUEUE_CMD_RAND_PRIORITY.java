package tirc.maf.test.cmd;

import java.util.Random;

import tirc.maf.MAFMessage;
import tirc.maf.QueuedCommand;

public class QUEUE_CMD_RAND_PRIORITY extends QueuedCommand {
	
	
	public QUEUE_CMD_RAND_PRIORITY() {
		Random rand = new Random(System.nanoTime());
		this.setPriority(rand.nextInt(256));
	}
	
	@Override
	public void execute() {
		MAFMessage outMsg = new MAFMessage();
		outMsg.setMessage(String.valueOf(this.getPriority()));
		this.send(outMsg);
	}
}
