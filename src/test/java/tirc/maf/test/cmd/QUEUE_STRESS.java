package tirc.maf.test.cmd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import tirc.maf.MAFMessage;
import tirc.maf.MAFServer;
import tirc.maf.QueuedCommand;

public class QUEUE_STRESS extends QueuedCommand {
		
	private static Logger logger = LoggerFactory.getLogger(QUEUE_STRESS.class) ;
	
	public QUEUE_STRESS() {
	}
	
	@Override
	public void execute() {
				
		//logger.debug("{}:{}", this.getInMessage().getClientId(),this.getInMessage().getMessage());
		
		if (0=="finish".compareToIgnoreCase(this.getOriginalMessage().getParamString())) {
			this.send(this.getOriginalMessage());	
			//System.out.println(this.getInMessage().getClientId()+":done!!!!");
		} else {
			MAFMessage message = new MAFMessage();
			message.setMessage(this.getOriginalMessage().getMessage()+","+this.getOriginalMessage().getClientId());
			this.broadcast(message);
		}
		
		

	}
}
