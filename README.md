#Message Application Framework (MAF)

This project help to rapidly develop client/server applications with asynchronous message communication.

Features:

1. Multi-Clients 
2. QueueCommand with priority setting
3. DirectCommand
4. Simple Heartbeat 
5. Unicast/Broadcast communication
6. Async message exchange 
7. Server/Client Application Framework